const express = require('express')
const validate = require('express-validation')
const handle = require('express-async-handler')

const routes = express.Router()

const services = require('./app/services')
const validators = require('./app/validators')

routes.post('/conversation', validate(validators.Message), handle(services.ConversationService.userMessage))

routes.get('/', ((async(req, res) => {

    res.json('Welcome to Watson Assistant Chatbot Nicky Fury that can talk about heroes API')

})));

module.exports = routes